-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 16 2017 г., 20:39
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `super_mag`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'рубашки', 1, 1),
(2, 'платья', 2, 1),
(3, 'брюки', 3, 1),
(4, 'шубы', 4, 1),
(5, 'майки', 5, 1),
(6, 'сапоги', 6, 1),
(8, 'нижнее белье', 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `price` double NOT NULL,
  `availability` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'temp.jpg',
  `description` text NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_recommendad` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `code`, `price`, `availability`, `brand`, `image`, `description`, `is_new`, `is_recommendad`, `status`) VALUES
(1, 'Куртка утепленная ', 2, 123123, 2500, 1, 'Modis', 'temp.jpg', 'Стеганая куртка Modis выполнена из гладкого текстиля. Модель прямого кроя с синтепоновым утеплителем. Детали: воротник-стойка, застежка на молнию, четыре внешних кармана и один карман внутри, текстильная подкладка.', 0, 0, 1),
(2, 'Толстовка', 1, 4545345, 1300, 1, 'Modis', 'temp.jpg', 'Толстовка Modis выполнена из плотного хлопкового трикотажа. Детали: прямой крой, застежка на молнию, мягкая флисовая подкладка, два кармана.', 1, 1, 1),
(3, 'Джинсы', 1, 123124, 1330, 1, 'Modis', 'temp.jpg', 'Джинсы Modis прямого кроя. Модель выполнена из хлопкового денима. Детали: застежка на пуговицу и молнию, пять внешних карманов.', 1, 1, 1),
(4, 'Джемпер', 4, 43423, 700, 0, 'Modis', 'temp.jpg', '', 0, 1, 1),
(5, 'Рубашка ', 2, 567, 1400, 0, 'Modis', 'temp.jpg', 'Рубашка от Modis приталенного кроя. Модель выполнена из мягкого текстиля. Детали: отложной воротник, застежка на пуговицы, пуговицы на манжетах, два кармана на груди.', 0, 0, 1),
(6, 'Брюки ', 3, 756, 2000, 1, 'Modis', 'temp.jpg', 'Брюки Modis выполнены из плотного хлопкового трикотажа. Детали: эластичный пояс и манжеты, модель без застежки, четыре кармана.', 1, 0, 1),
(7, 'Футболка ', 1, 564, 400, 1, 'Modis', 'temp.jpg', '', 0, 0, 1),
(8, 'Футболка ', 1, 23411, 350, 1, 'Modis', 'temp.jpg', '', 0, 0, 1),
(9, 'Лонгслив ', 1, 11112222, 650, 0, 'Modis', 'temp.jpg', '', 0, 0, 1),
(10, 'Джемпер ', 1, 9832, 800, 1, 'Modis', 'temp.jpg', '', 0, 0, 1),
(11, 'Джемпер ', 1, 46798, 700, 1, 'Modis', 'temp.jpg', '', 0, 0, 1),
(12, 'Куртка M NSW AV15 JKT FLC', 1, 233111, 5000, 1, 'Nike', 'temp.jpg', 'Мужская куртка Nike Sportswear Advance 15 обеспечивает комфорт. Флис двойной вязки с ткаными накладками на рукавах и нагрудном кармане. Детали: рукава-регланы не сковывают движений; открытые карманы спереди и нагрудный карман на молнии - для удобного хранения мелочей; полноразмерная молния позволяет удобно переодеваться; манжеты и нижняя кромка из рубчатой ткани - для комфортной посадки; на груди слева нанесен крупный принт с логотипом Nike.', 0, 0, 1),
(13, 'Ветровка M NSW WR JKT', 1, 23131414, 6000, 1, 'Nike', 'temp.jpg', 'Традиции Nike Windrunner продолжает мужская куртка Nike Windrunner в новом красочном исполнении. Модель выполнена из легкой ткани с контрастным цветовым решением. Легкая ткань рипстоп обеспечивает прочность. Капюшон из нескольких панелей с козырьком и утягивающими шнурками дарит тепло и защищает от непогоды. Нашивка выполнена в классическом красочном стиле Windrunner. Вырез на задней кокетке обеспечивает дополнительную воздухопроницаемость. Карманы на молнии выполнены в контрастном цвете и удобны для хранения мелочей.', 0, 1, 1),
(15, 'Кроссовки NIKE AIR VERSITILE', 1, 753159, 5700, 1, 'Nike', 'temp.jpg', '', 0, 0, 1),
(19, 'САПОГИ RENZONI ', 6, 826698, 23140, 1, 'RENZONI ', 'temp.jpg', 'Сезон: Еврозима\r\nЦвет: Черный\r\nВнешний материал: Натуральная замша\r\nВнутренний материал: Натуральная кожа/натуральный мех\r\nСтелька: Натуральный мех\r\nПодошва: Полимер', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `products` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_order`
--

INSERT INTO `product_order` (`id`, `user_name`, `user_phone`, `user_comment`, `user_id`, `date`, `products`, `status`) VALUES
(1, 'серега', '+79887042414', 'привет', 0, '2017-03-02 11:50:47', '{"14":2,"15":1,"13":1}', 2),
(5, 'Сергей', '+79887042414', 'тест', 2, '2017-03-13 15:03:49', '{"19":1,"12":1,"11":1}', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'vasya', 'vasya@mail.ru', '111222', ''),
(2, 'Сергей', 'serega_pro@inbox.ru', '555555', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
