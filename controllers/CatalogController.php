<?php

class CatalogController
{
    public function actionIndex()
    {
        $categories = array();
        $categories = category::getCategoriesList();

        $latestProduct = array();
        $latestProduct = Product::getLatesProducts(6);

        require_once (ROOT . '/views/catalog/index.php');
        return true;
    }

    public function actionCategory($categoryId, $page = 1)
    {
        $categories = array();
        $categories = category::getCategoriesList();

        $categoryProduct = array();
        $categoryProduct = Product::getProductsListByCategory($categoryId, $page);

        $total = Product::getTotalProductsInCategory($categoryId);

        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');

        require_once (ROOT . '/views/catalog/category.php');
        return true;
    }
}