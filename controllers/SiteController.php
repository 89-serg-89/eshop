<?php

class SiteController
{
    public function ActionIndex()
    {
        $categories = array();
        $categories = category::getCategoriesList();

        $latestProduct = array();
        $latestProduct = Product::getLatesProducts(6);

        $recomendateProduct = array();
        $recomendateProduct = Product::getRecomedateProduct();

        require_once (ROOT . '/views/site/index.php');
        return true;
    }

    public function actionContact()
    {
        $userEmail = '';
        $userText = '';
        $result = false;

        if(isset($_POST['submit'])) {
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;

            // Валидация полей
            if(!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }

            if($errors == false) {
                $adminEmail = 'serega_pro@inbox.ru';
                $messege = "Текст: {$userText}. От {$userEmail}";
                $subject = 'Письмо с обратной связи';
                $result = mail($adminEmail, $subject, $messege);
                $result = true;
            }
        }
        require_once (ROOT . '/views/site/contact.php');
        return true;
    }
    
    /**
     * Action для страницы "О магазине"
     */
    public function actionAbout()
    {
        // Подключаем вид
        require_once(ROOT . '/views/site/about.php');
        return true;
    }
}