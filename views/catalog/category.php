<?php include_once (ROOT . '/views/layout/header.php')?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Каталог</h2>
                        <div class="panel-group category-products">
                            <?php foreach ($categories as $categoryItem):?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="/category/<?php echo $categoryItem['id']?>"
                                               class="<?php if($categoryId[1] == $categoryItem['id']) echo 'active'; ?>"
                                            >
                                                <?php echo $categoryItem['name'];?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>

                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <h2 class="title text-center">Последние товары</h2>
                    <div class="features_items"><!--features_items-->

                        <?php foreach ($categoryProduct as $categoryItem):?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="<?php echo Product::getImage($categoryItem['id'])?>" alt="" />
                                            <h2><?php echo $categoryItem['price'];?> руб.</h2>
                                            <a href="/product/<?php echo $categoryItem['id'];?>">
                                                <p><?php echo $categoryItem['name'];?></p>
                                            </a>
                                            <a href="#" data-id="<?php echo $categoryItem['id']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                        </div>
                                        <?php if($categoryItem['is_new']):?>
                                            <img src="/template/images/home/new.png" class="new" alt="" />
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>

                    </div><!--features_items-->
                    <?php echo $pagination->get(); ?>
                </div>
            </div>
        </div>
    </section>
<?php include_once (ROOT . '/views/layout/footer.php')?>